#include <iostream>
#include <cmath>
using namespace std;

int project1()
{
	int sum  = 0;
	for (int i = 0; i < 1000; i++)
	{
		if (i % 3 == 0)
		{	
			sum += i;
		}
		else if(i % 5 == 0)
		{
			sum += i;
		}
	}
	return sum;
}

long int project2()
{
	long int sum = 0;
	long int fib = 1;
	long int oldfib = 0;
	while(fib <= 4000000)
	{
		if(fib % 2 == 0)
		{
			sum += fib;
		}

		long int tmp = fib;
		fib = fib + oldfib;
		oldfib = tmp;
		cout << fib << endl;
		
	}
	return sum;
	
}

void project3()
{
	long int fact = 600851475143;
	for (long int i = 1; i < sqrt(fact); i++)
	{	
		if(fact % i == 0)
		{
			cout << i << endl;

		}
	}		
}

void project4()
{
	cout << "See python" << endl;
}

long int project5()
{	
	int i = 1;
	while(i)
	{
		int counter = 0;
		for (int j = 1; j <= 20; j++)
		{	
			if (i % j == 0)
			{
				counter++;
			}
			if(counter == 20)
			{
				return i;
			}
		}
		i++;			
	}
}

long int project6()
{
	long int sum1 = 0;
	long int sum2 = 0;

	for (int i = 1; i <= 100; i++)
	{
		sum1 += i*i;
	}

	for (int i = 1; i <= 100; i++)
	{
		sum2 += i;
	}
	
	sum2 = sum2 * sum2;
	return sum2 - sum1;
}

void project7()
{
	long int prime = 2;
	for(int i = 1; i <= 10001; i = i)
	{
		bool notPrime = false;
		for(int j = 2; j < prime; j++)
		{	
			if(prime % j == 0)
			{
				notPrime = true;
				break;			
			}
		}
		
		if(!notPrime)
		{
			cout << i << " " << prime << endl; 
			i++;
		}
		prime++;
	}
}

void project8()
{
	cout << "See python" << endl;
}


int project9()
{
	for(int i = 1; i < 1000; i++)
	{
		int aa = i*i;
		for(int j = 1; j < 1000; j++)
		{
			int bb = j*j;
			for(int k = 1; k < 1000; k++)
			{
				int cc = k*k;
				if (aa + bb == cc)
				{
					if (i + j + k == 1000)
					{
						return i*j*k;
					}
				}
			}			
		}
	}
}
long int project10()
{
	//Start in 5 since I did not manage to get 
	//the first two primenumbers, 3 and 2.
	long int sum = 5;
	for (int i = 2; i < 2000000; i++)
	{
		for(int j = 2; j*j <= i; j++)
		{
			if (i % j == 0)
			{
				break;
			}
			else if (j + 1 > sqrt(i))
			{	
				sum += i;				
			}		
		}
	}
	return sum;
}
int project12()
{
//76576500
	int sum = 0;
	int counter = 1;
	for (int i = 1; i < 1000000;  i++)
	{	
		sum +=i;
		for (int j = 1; j <= i; j++)
		{
			if (sum % j == 0)
			{
				counter++;
			}
			
			if (counter > 300)
			{
				return sum;
			}
		}
		counter = 1;
	}
	return -1;
}


int main()
{
	cout << project12() << endl;
	return 0;
}
